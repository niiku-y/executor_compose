# Makefile

AP := executor_compose
VER := 1.0
TAG = $(AP):$(VER)
REG := registry.gitlab.com
USR := niiku-y
REG_TAG = $(REG)/$(USR)/$(TAG)

build:
	sudo docker build \
	  --build-arg http_proxy=${http_proxy} \
	  --build-arg https_proxy=${https_proxy} \
	  --build-arg no_proxy=${no_proxy} \
	  --build-arg HTTP_PROXY=${HTTP_PROXY} \
	  --build-arg HTTPS_PROXY=${HTTPS_PROXY} \
	  --build-arg NO_PROXY=${NO_PROXY} \
	  -f ./Dockerfile -t $(TAG) .

push:
	sudo docker login $(REG)
	sudo docker tag $(TAG) $(REG_TAG)
	sudo docker push $(REG_TAG)

run:
	sudo docker run -d -it $(TAG)

imgs:
	sudo docker images

ps:
	sudo docker ps -a

clean:
	sudo docker rm `sudo docker ps -f "status=exited" -q`

