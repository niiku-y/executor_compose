# Dockerfile
FROM alpine:3.10 AS stage1

ARG http_proxy
ARG https_proxy
ARG no_proxy
ARG HTTP_PROXY
ARG HTTPS_PROXY
ARG NO_PROXY

# proxy
ENV http_proxy=${http_proxy}
ENV https_proxy=${https_proxy}
ENV no_proxy=${no_proxy}
ENV HTTP_PROXY=${HTTP_PROXY}
ENV HTTPS_PROXY=${HTTPS_PROXY}
ENV NO_PROXY=${NO_PROXY}

# resource
ENV go_ver=go1.12.6.linux-amd64
ENV go_tgz=${go_ver}.tar.gz
ENV go_url=https://dl.google.com/go/${go_tgz}
ENV go_path=/go

# build
RUN apk update && \
    apk --no-cache add \
	bash \
        curl \
	git \
	make \
	openssl \
	tar \
	&& \
    apk --no-cache add --virtual .build-deps \
	gcc \
        go \
	musl-dev \
	openssl-dev \
	&& \
    if [ "${http_proxy}" == "" ]; then \
        curl -L $go_url -o $go_tgz ; \
    else \
        curl -L $go_url -x $http_proxy -o $go_tgz ; \
    fi \
    && \
    tar -C /usr/local -xf $go_tgz && \
    cd /usr/local/go/src && \
    ./make.bash && \
    apk del .build-deps

FROM docker:18.09 AS stage2

COPY --from=stage1 /usr/local/go/bin/go /usr/local/go/bin/go

# resource
ENV compose_url=https://github.com/docker/compose/releases/download/1.24.1/docker-compose-Linux-x86_64
ENV GOPATH=/go
ENV PATH="$GOPATH/bin:/usr/local/go/bin:$PATH"

RUN apk update && \
    apk --no-cache add \
	bash \
        curl \
	git \
	make \
	openssl \
	python3 \
	py-pip \
	tar \
	&& \
    apk --no-cache add --virtual .build-deps \
        gcc \
	libffi-dev \
	musl-dev \
	openssl-dev \
	python3-dev \
	&& \
    if [ "${http_proxy}" == "" ]; then \
        pip3 --no-cache-dir install --upgrade pip && \
        pip3 --no-cache-dir install docker-compose ; \
    else \
        pip3 install --upgrade pip  --proxy=${http_proxy} && \
        pip3 install docker-compose --proxy=${http_proxy} ; \
    fi \
    && \
    apk del .build-deps && \
    rm -rf /var/cache/apk/* && \
    mkdir -p $GOPATH && \
    echo "export PATH=$PATH" >> /etc/profile && \
    echo "export GOPATH=$GOPATH" >> /etc/profile && \
    if [ "${http_proxy}" != "" ]; then \
        git config --global http.proxy  ${http_proxy}  && \
        git config --global https.proxy ${https_proxy} ;  \
    fi \
    && \
    go version && \
    docker-compose version

CMD ["bash"]

